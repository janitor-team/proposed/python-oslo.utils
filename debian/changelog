python-oslo.utils (6.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 13:59:54 +0200

python-oslo.utils (6.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 15:27:22 +0200

python-oslo.utils (4.12.3-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 13 May 2022 10:45:32 +0200

python-oslo.utils (4.12.2-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 09:26:23 +0100

python-oslo.utils (4.12.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 19 Feb 2022 16:10:16 +0100

python-oslo.utils (4.10.1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 18 Jan 2022 17:02:21 +0100

python-oslo.utils (4.10.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:03:15 +0200

python-oslo.utils (4.10.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 13:30:30 +0200

python-oslo.utils (4.8.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 08:49:24 +0200

python-oslo.utils (4.8.0-2) experimental; urgency=medium

  * Fixed (build-)depends for Bullseye release.
  * Bump Standards-Version to 4.5.1.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Mar 2021 18:32:17 +0100

python-oslo.utils (4.8.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Mar 2021 18:24:46 +0100

python-oslo.utils (4.6.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 22:48:51 +0200

python-oslo.utils (4.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed disable-unit-test-that-fails-with-py3.8.patch.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 15:46:31 +0200

python-oslo.utils (4.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed debian/patches/Fix_uuidsentinel_to_follow_getattr_protocol.patch
    applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 12:32:48 +0200

python-oslo.utils (4.1.2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Jul 2020 10:50:00 +0200

python-oslo.utils (4.1.1-4) unstable; urgency=medium

  * Add Fix_uuidsentinel_to_follow_getattr_protocol.patch.

 -- Thomas Goirand <zigo@debian.org>  Sat, 27 Jun 2020 10:56:13 +0200

python-oslo.utils (4.1.1-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 21:32:19 +0200

python-oslo.utils (4.1.1-2) experimental; urgency=medium

  * Do not run tests with specific Python version, use all versions that are
    available (Closes: #958032).

 -- Thomas Goirand <zigo@debian.org>  Sat, 18 Apr 2020 00:44:38 +0200

python-oslo.utils (4.1.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 14:11:05 +0200

python-oslo.utils (3.41.5-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Mar 2020 12:47:54 +0100

python-oslo.utils (3.41.3-1) unstable; urgency=medium

  * CVE-2019-3866: Sensitive information leaked in mistral logs. Upgraded to
    latest upstream point release, including upstream patch: Make mask_password
    case insensitive, and add new patterns (Closes: #946060).
  * Add patch to disable a failing unit test in Python 3.8. Issue is reported
    upstream at: https://bugs.launchpad.net/oslo.utils/+bug/1857199.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Dec 2019 20:57:21 +0100

python-oslo.utils (3.41.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Fix wrong Vcs-*.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 00:56:22 +0200

python-oslo.utils (3.41.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Oct 2019 08:48:22 +0200

python-oslo.utils (3.41.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 12 Sep 2019 09:40:43 +0200

python-oslo.utils (3.41.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Sep 2019 11:16:06 +0200

python-oslo.utils (3.40.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 00:58:05 +0200

python-oslo.utils (3.40.3-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Mar 2019 20:26:05 +0100

python-oslo.utils (3.36.4-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 23:20:56 +0200

python-oslo.utils (3.36.4-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release (rocky).
  * Fixed (build-)depends for this release.

  [ Michal Arbet ]
  * d/control: Bump debhelper version

 -- Thomas Goirand <zigo@debian.org>  Sat, 18 Aug 2018 21:46:12 +0200

python-oslo.utils (3.35.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:28:49 +0000

python-oslo.utils (3.35.0-1) experimental; urgency=medium

  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Update (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Sat, 10 Feb 2018 14:22:22 +0100

python-oslo.utils (3.28.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed watch file.
  * Removed disabling dh_sphinxdoc.
  * Standards-Version is now 4.1.1.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 21:56:33 +0000

python-oslo.utils (3.28.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed now useless transition packages.
  * debian/gpb.conf using debian/pike.
  * Removed now useless requirements patch for pytz version.
  * Disable calling dh_sphinxdoc as it's broken.

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Aug 2017 18:13:37 +0000

python-oslo.utils (3.8.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 08:29:15 +0000

python-oslo.utils (3.8.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Apr 2016 14:46:26 +0200

python-oslo.utils (3.7.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * Fixed minimum version of python{3,}-netifaces to 0.10.4.
  * Standards-Version: 3.9.7 (no change).

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).

  [ Corey Bryant ]
  * New upstream release.
  * d/control: Align (Build-)Depends with upstream.
  * d/p/allow-any-pytz-version.patch: Rebased.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2016 10:22:30 +0000

python-oslo.utils (3.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed ordering in debian/coypright.
  * Fixed oslo.config version in build-depends-indep.
  * Fixed duplicate long description.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Jan 2016 08:33:00 +0000

python-oslo.utils (3.3.0-1) experimental; urgency=medium

  [ David Della Vecchia ]
  * New upstream release.
  * d/control: Align requirements with upstream.

  [ Corey Bryant ]
  * d/control: Add python3-funcsigs to (build-)depends.
  * d/rules: Run tests for all py3 versions.

  [ Thomas Goirand ]
  * Added Corey as Uploaders:.

 -- Corey Bryant <corey.bryant@canonical.com>  Fri, 08 Jan 2016 12:56:27 -0500

python-oslo.utils (3.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Dec 2015 10:39:35 +0100

python-oslo.utils (2.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Switched from override_dh_install to override_dh_auto_install.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 20:47:59 +0000

python-oslo.utils (2.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Refreshed patches.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 21:45:21 +0200

python-oslo.utils (2.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Added a patch to allow any version of pytz.
  * Do not run test using Python 3.5 (python-netifaces can't be imported).

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Sep 2015 09:58:33 +0000

python-oslo.utils (2.1.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed && -> ; in debian/rules.
  * Fixed (build-)depends for this release.
  * Do not manually copy oslo/utils, as the namespace version is gone.

  [ James Page ]
  * Fixup typo in transitional package description (LP: #1471561).

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Jun 2015 23:45:52 +0200

python-oslo.utils (1.6.0-2) experimental; urgency=medium

  * Team upload.
  * d/control: Remove errant , from Uploaders field.
  * d/watch: Update to use Debian pypi redirector.

 -- James Page <james.page@ubuntu.com>  Mon, 15 Jun 2015 08:43:09 +0100

python-oslo.utils (1.6.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release:
    - d/control: Align dependencies and version requirements with upstream.
    - d/control: Drop Build-Conflicts on python{3}-oslo.utils, no longer
      needed.
  * Re-align with Ubuntu:
    - d/control: Add Breaks/Replaces for Ubuntu *oslo-utils packages.
    - d/control: Add transitional packages for *oslo-utils.

 -- James Page <james.page@ubuntu.com>  Tue, 09 Jun 2015 08:50:43 +0100

python-oslo.utils (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Feb 2015 18:14:13 +0100

python-oslo.utils (1.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Reviewed (build-)depends.
  * Added patch of .testr.conf to not list ./tests unit tests, which are using
    the oslo. namespace (and then, failing in Python 3).

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Dec 2014 13:06:27 +0800

python-oslo.utils (1.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Sep 2014 14:52:27 +0000

python-oslo.utils (0.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed new (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Sep 2014 12:20:48 +0800

python-oslo.utils (0.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Uploading to experimental before Jessie's freeze.

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Sep 2014 15:50:43 +0800

python-oslo.utils (0.1.1-1) unstable; urgency=medium

  * Initial release. (Closes: #757325)

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Aug 2014 15:40:04 +0800
